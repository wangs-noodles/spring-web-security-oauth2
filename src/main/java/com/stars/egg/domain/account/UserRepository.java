package com.stars.egg.domain.account;

/**
 * @author wangs
 */
public interface UserRepository {
    User findByUsername(String username);
}

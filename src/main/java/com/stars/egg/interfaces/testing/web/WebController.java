package com.stars.egg.interfaces.testing.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

/**
 * @author wangs
 * @since 2018/12/19 10:21
 */
@Controller
public class WebController {

    /** Logger. */
    private final Logger logger = LoggerFactory.getLogger(WebController.class);

    @GetMapping(value = "/login")
    public String login(){
        return "login";
    }

    @GetMapping(value = {"/"})
    public String goUserPage(){
        return "forward:user";
    }

    @GetMapping(value = {"/user"})
    public String main(Principal principal){
        logger.info("Principal: {}", principal.getName());
        return "user/user";
    }

    @GetMapping("/403")
    public String showNotHavePermissionPage(){
        return "/error/403";
    }
}

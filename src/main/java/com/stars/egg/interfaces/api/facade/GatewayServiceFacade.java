package com.stars.egg.interfaces.api.facade;

import com.stars.egg.infrastructure.exception.PlatformError;
import com.stars.egg.infrastructure.exception.PlatformException;
import com.stars.egg.interfaces.api.rest.RestJsonResponseErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * API Gateway
 *
 * @author wangs
 */
@Component
public class GatewayServiceFacade {
    /** Logger. */
    private final Logger logger = LoggerFactory.getLogger(GatewayServiceFacade.class);

    private final RestTemplate gatewayRestTemplate;

    private final OAuth2ProtectedResourceDetails resourceDetails;

    private final ClientHttpRequestFactory clientHttpRequestFactory;

    private final RestJsonResponseErrorHandler errorHandler;

    @Autowired
    public GatewayServiceFacade(
            ClientHttpRequestFactory clientHttpRequestFactory,
            RestTemplate gatewayRestTemplate,
            OAuth2ProtectedResourceDetails resourceDetails,
            RestJsonResponseErrorHandler errorHandler) {
        this.clientHttpRequestFactory = clientHttpRequestFactory;
        this.errorHandler = errorHandler;
        Assert.notNull(gatewayRestTemplate, "gatewayRestTemplate can not be null");
        Assert.notNull(resourceDetails, "resourceDetails can not be null");
        Assert.notNull(clientHttpRequestFactory, "clientHttpRequestFactory can not be null");
        this.resourceDetails = resourceDetails;
        this.gatewayRestTemplate = gatewayRestTemplate;
    }

    /**
     * Perform a password oauth2 authentication with local authorization server.
     *
     * @param username user name.
     * @param password user password.
     * @return oauth2 token.
     */
    public OAuth2AccessToken authenticate(String username, String password) {
        Assert.hasText(username, "username can not be blank");
        Assert.hasText(password, "password can not be blank");

        MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
        parameters.add("grant_type", "password");
        parameters.add("username", username);
        parameters.add("password", password);


        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(parameters, null);

        try {
            ResponseEntity<OAuth2AccessToken> response =
                    gatewayRestTemplate.exchange(
                            resourceDetails.getAccessTokenUri(),
                            HttpMethod.POST,
                            request,
                            OAuth2AccessToken.class);

            OAuth2AccessToken accessToken = response.getBody();

            if (accessToken != null) {
                logger.info("Authenticated: {}", accessToken);
            }

            return accessToken;
        } catch (PlatformException e) {
            logger.info("login failure {}", e.getMessage());
            throw PlatformException.of(PlatformError.OAUTH2_BAD_CREDENTIALS, e.getMessage());
        }
    }

    /**
     * Refresh access token.
     *
     * @param refreshToken refresh token.
     * @return access token
     */
    public OAuth2AccessToken refreshToken(String refreshToken) {
        Assert.hasText(refreshToken, "refreshToken can not be blank");

        MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
        parameters.add("grant_type", "refresh_token");
        parameters.add("refresh_token", refreshToken);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(parameters, null);
        ResponseEntity<OAuth2AccessToken> response =
                gatewayRestTemplate.exchange(
                        resourceDetails.getAccessTokenUri(),
                        HttpMethod.POST,
                        request,
                        OAuth2AccessToken.class);

        OAuth2AccessToken accessToken = response.getBody();

        if (accessToken != null) {
            logger.info("Refreshed: {}", accessToken);
        }

        return accessToken;
    }
}

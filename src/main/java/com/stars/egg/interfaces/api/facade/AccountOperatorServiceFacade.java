package com.stars.egg.interfaces.api.facade;

import com.stars.egg.domain.account.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

/**
 * Account Operator Service
 *
 * @author wangs
 */
@Service
public class AccountOperatorServiceFacade {

    private final UserRepository userRepository;

    private final ConsumerTokenServices consumerTokenServices;

    private final TokenStore tokenStore;

    @Autowired
    public AccountOperatorServiceFacade(
            UserRepository userRepository,
            ConsumerTokenServices consumerTokenServices,
            TokenStore tokenStore) {
        Assert.notNull(userRepository, "userRepository can not be null");
        this.userRepository = userRepository;
        this.consumerTokenServices = consumerTokenServices;
        this.tokenStore = tokenStore;
    }


    public void removeToken(String name, String clientId) {
        Assert.hasText(clientId, "clientId can not be blank.");
        Assert.hasText(name, "name can not be blank.");
        tokenStore.findTokensByClientIdAndUserName(clientId, name).forEach(tokenR -> consumerTokenServices.revokeToken(tokenR.getValue()));
    }
}

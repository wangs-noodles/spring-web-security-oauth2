package com.stars.egg.interfaces.api.rest;

import com.stars.egg.infrastructure.exception.PlatformError;
import org.springframework.security.oauth2.client.resource.OAuth2AccessDeniedException;
import org.springframework.security.oauth2.common.exceptions.*;
import org.springframework.util.Assert;

import java.util.stream.Stream;

/**
 * OAuth2 exception translator.
 *
 * <p>Provides facility to convert oauth2 exceptions to platform errors.
 * @author wangs
 */
public class OAuth2ExceptionTranslator {
    /**
     * Internal translator table entry.
     */
    private static final class Entry {
        Class<? extends OAuth2Exception> exClass;
        PlatformError platformError;
        static Entry of(Class<? extends OAuth2Exception> exClass, PlatformError error) {
            Entry entry = new Entry();
            entry.exClass = exClass;
            entry.platformError = error;
            return entry;
        }
    }

    /**
     * Predefined translate table.
     */
    private static final Entry[] ENTRIES = {
            Entry.of(InvalidClientException.class, PlatformError.OAUTH2_INVALID_CLIENT),
            Entry.of(InvalidGrantException.class, PlatformError.OAUTH2_INVALID_GRANT),
            Entry.of(InvalidRequestException.class, PlatformError.OAUTH2_INVALID_REQUEST),
            Entry.of(InvalidTokenException.class, PlatformError.OAUTH2_INVALID_TOKEN),
            Entry.of(RedirectMismatchException.class, PlatformError.OAUTH2_REDIRECT_URI_MISMATCH),
            Entry.of(UnauthorizedClientException.class, PlatformError.OAUTH2_UNAUTHORIZED_CLIENT),
            Entry.of(InsufficientScopeException.class, PlatformError.OAUTH2_INSUFFICIENT_SCOPE),
            Entry.of(OAuth2AccessDeniedException.class, PlatformError.OAUTH2_ACCESS_DENIED),
            Entry.of(UnauthorizedUserException.class, PlatformError.OAUTH2_UNAUTHORIZED_USER),
            Entry.of(InvalidScopeException.class, PlatformError.OAUTH2_INVALID_SCOPE),
            Entry.of(UnsupportedGrantTypeException.class, PlatformError.OAUTH2_UNSUPPORTED_GRANT_TYPE),
            Entry.of(UnsupportedResponseTypeException.class, PlatformError.OAUTH2_UNSUPPORTED_RESPONSE_TYPE),
            Entry.of(UserDeniedAuthorizationException.class, PlatformError.OAUTH2_USER_DENIED_AUTHORIZATION)
    };

    /**
     * Translate a oauth2 exception to a platform error.
     * @param exception oauth2 exception
     * @return platform error
     */
    public static PlatformError translate(OAuth2Exception exception) {
        Assert.notNull(exception, "oauth2 exception can not be null");

        return Stream.of(ENTRIES)
                .filter(e -> e.exClass.equals(exception.getClass()))
                .findFirst()
                .orElseThrow(RuntimeException::new)
                .platformError;
    }
}

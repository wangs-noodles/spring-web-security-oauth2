package com.stars.egg.interfaces.api.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangs
 * @since 2018/12/19 10:12
 */
@RestController
@RequestMapping("/api/v1")
public class Oauth2TestController {

    /** Logger. */
    private final Logger logger = LoggerFactory.getLogger(Oauth2TestController.class);

    @RequestMapping("/test")
    @PreAuthorize("hasRole('ADMIN')")
    public String oauth2Test() {
        return "Oauth2 authentication success!";
    }
}

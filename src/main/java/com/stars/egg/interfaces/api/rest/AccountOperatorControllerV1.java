package com.stars.egg.interfaces.api.rest;

import com.stars.egg.infrastructure.exception.PlatformError;
import com.stars.egg.infrastructure.exception.PlatformException;
import com.stars.egg.interfaces.api.facade.AccountOperatorServiceFacade;
import com.stars.egg.interfaces.api.facade.GatewayServiceFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.Optional;

/**
 * Account Operator Controller
 *
 * @author wangs
 */
@RestController
@RequestMapping("/api/v1")
public class AccountOperatorControllerV1 {
    private final Logger logger = LoggerFactory.getLogger(AccountOperatorControllerV1.class);

    private final AccountOperatorServiceFacade service;

    private final GatewayServiceFacade gateway;

    @Autowired
    public AccountOperatorControllerV1(AccountOperatorServiceFacade service, GatewayServiceFacade gateway) {
        Assert.notNull(service, "AccountOperatorServiceV1 can not be null.");
        Assert.notNull(gateway, "Gateway can not be null.");
        this.service = service;
        this.gateway = gateway;
    }

    /**
     * Authenticate user credentials against local oauth2 server.
     *
     * @param username username.
     * @param password user password.
     * @return oauth2 access token if succeed.
     */
    @RequestMapping(value = "/auth", method = RequestMethod.POST)
    public OAuth2AccessToken authenticate(
            @RequestParam String username,
            @RequestParam String password) {
        OAuth2AccessToken accessToken = gateway.authenticate(username, password);

        logger.info("accessToken: {}", accessToken);
        return accessToken;
    }

    /**
     * Authenticate user credentials against local oauth2 server.
     *
     * @param refreshToken refreshToken can not be blank.
     * @return oauth2 access token if succeed.
     */
    @RequestMapping(value = "/token", method = RequestMethod.POST)
    public OAuth2AccessToken authenticate(@RequestParam String refreshToken) {
        OAuth2AccessToken accessToken = gateway.refreshToken(refreshToken);
        logger.info("refreshed accessToken: {}", accessToken);
        return accessToken;
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public void register(Principal principal, @RequestParam String clientId){
        String username = Optional.ofNullable(principal.getName())
                .orElseThrow(() -> PlatformException.of(PlatformError.PRINCIPAL_IS_NULL));
        logger.info("{} logout", username);
        service.removeToken(principal.getName(), clientId);
    }
}

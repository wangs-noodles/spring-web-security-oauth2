package com.stars.egg.interfaces.api.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stars.egg.infrastructure.exception.PlatformError;
import com.stars.egg.infrastructure.exception.PlatformException;
import com.stars.egg.infrastructure.exception.SystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.security.authentication.AccountStatusException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.oauth2.client.http.OAuth2ErrorHandler;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

/**
 * REST response error handler
 * @author wangs
 */
@Component
public class RestJsonResponseErrorHandler implements ResponseErrorHandler {
    /** Logger. */
    private final Logger logger = LoggerFactory.getLogger(RestJsonResponseErrorHandler.class);

    /** Jackson object mapper. */
    private final ObjectMapper objectMapper;

    private final OAuth2ErrorHandler oAuth2ErrorHandler;

    @Autowired
    public RestJsonResponseErrorHandler(
            OAuth2ProtectedResourceDetails resourceDetails) {
        Assert.notNull(resourceDetails, "oauth2 resource not configured");
        objectMapper = new ObjectMapper().findAndRegisterModules();
        oAuth2ErrorHandler = new OAuth2ErrorHandler(resourceDetails);
    }

    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        return oAuth2ErrorHandler.hasError(response);
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        try {
            oAuth2ErrorHandler.handleError(response);
        }  catch (HttpStatusCodeException e) {
            String json = e.getResponseBodyAsString();
            if (!StringUtils.hasText(json)) {
                /* REST error without entity body.
                 * Normally it happens when RPC (via REST APIs) without authorization
                 * or invalid request, and those errors should be avoid by this endpoint.
                 * So it those errors occurred, throw a system exception from here.
                 */
                throw SystemException.internal(e.getStatusCode().name());
            }
            if(e.getStatusCode()== HttpStatus.UNAUTHORIZED) {
                throw PlatformException.of(PlatformError.OAUTH2_INVALID_TOKEN);
            }
            try {
                RestError restError = objectMapper.readValue(json, RestError.class);
                logger.error("[API-Service] REST error, {}", json);
                throw PlatformException.of(PlatformError.of(restError.getCode()), restError.toString());
            }catch (IOException e2) {
                logger.error("Failed to parse response", e2);
                throw SystemException.internal(e2);
            }
        } catch (BadCredentialsException e) {
            throw PlatformException.of(PlatformError.OAUTH2_BAD_CREDENTIALS);
        } catch (AccountStatusException e) {
            throw PlatformException.of(PlatformError.AUTH_ACCOUNT_STATUS);
        } catch (InsufficientAuthenticationException e) {
            throw PlatformException.of(PlatformError.AUTH_INSUFFICIENT_AUTHENTICATION);
        } catch (OAuth2Exception e) {
            throw PlatformException.of(OAuth2ExceptionTranslator.translate(e), e.getMessage());
        }
    }
}

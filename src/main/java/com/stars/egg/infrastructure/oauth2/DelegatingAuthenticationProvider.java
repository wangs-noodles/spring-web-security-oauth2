package com.stars.egg.infrastructure.oauth2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;



/**
 * @author wangs
 */
@Component
public class DelegatingAuthenticationProvider implements AuthenticationProvider {

    private final Logger logger = LoggerFactory.getLogger(DelegatingAuthenticationProvider.class);

    private UserDetailsService userDetailsService;

    @Autowired
    public DelegatingAuthenticationProvider(UserDetailsService userDetailsService) {
        Assert.notNull(userDetailsService, "userDetailsService can not be null");
        this.userDetailsService = userDetailsService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = (String) authentication.getCredentials();
        if (this.logger.isDebugEnabled()) {
            this.logger.debug("Processing authentication request for user: " + username);
        }

        if (!StringUtils.hasLength(username)) {
            throw new BadCredentialsException("Empty username");
        }

        if (!StringUtils.hasLength(password)) {
            throw new BadCredentialsException("Empty Password");
        }

        Assert.notNull(password, "Null password was supplied in authentication token");
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);


        if(ObjectUtils.isEmpty(userDetails)){
            throw new BadCredentialsException(String.format("User %s is not exists.", username));
        }
        if (username.equals(userDetails.getUsername()) && password.equals(userDetails.getPassword()))
            return new UsernamePasswordAuthenticationToken
                    (username, password, userDetails.getAuthorities());
        else {
            throw new BadCredentialsException("Password error.");
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}

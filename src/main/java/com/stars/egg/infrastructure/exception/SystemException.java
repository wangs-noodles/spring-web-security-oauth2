package com.stars.egg.infrastructure.exception;

/**
 * System exception.
 * @author wangs
 */
public abstract class SystemException extends RuntimeException {
    public SystemException(String message) {
        super(message);
    }

    public SystemException(Throwable cause) {
        super(cause);
    }

    public SystemException(String message, Throwable cause) {
        super(message, cause);
    }

    public static SystemException internal(String message) {
        return new SystemException(message) {};
    }

    public static SystemException internal(Throwable cause) {
        return new SystemException(cause) {};
    }

    public static SystemException internal(String message, Throwable cause) {
        return new SystemException(message, cause) {};
    }
}

package com.stars.egg.infrastructure.exception;

import java.util.stream.Stream;

/**
 * Platform error.
 * @author wangs
 */
public enum PlatformError {
    /**
     * system error
     * code [1~999]
     */
    BAD_REQUEST(400, "Bad request"),
    NO_ACCESS_RIGHTS(403, "No access rights"),
    SERVER_INTERNAL_ERROR(500, "Server internal error"),
    INITIALIZE_MAIL_SSL_FAIL(600, "Initialize mail ssl fail"),
    EXPORT_ASSERT_EXCEL_FAIL(601, "Export assert excel fail"),
    EXPORT_Driving_LICENSE_EXCEL_FAIL(601, "Export driving license excel fail"),
    GENERATE_DOCUMENT_ERROR(602, "Generate document error."),


    /**
     * server error
     * code [1000~1999]
     */
    OAUTH2_INVALID_REQUEST(1001, "Invalid request"),
    OAUTH2_INVALID_CLIENT(1002, "Invalid client"),
    OAUTH2_INVALID_GRANT(1003, "Invalid grant"),
    OAUTH2_UNAUTHORIZED_CLIENT(1004, "Unauthorized client"),
    OAUTH2_UNSUPPORTED_GRANT_TYPE(1005, "Unsupported grant type"),
    OAUTH2_INVALID_SCOPE(1006, "Invalid scope"),
    OAUTH2_INSUFFICIENT_SCOPE(1007, "Insufficient scope"),
    OAUTH2_INVALID_TOKEN(1008, "Invalid token"),
    OAUTH2_REDIRECT_URI_MISMATCH(1009, "Redirect uri mismatch"),
    OAUTH2_UNSUPPORTED_RESPONSE_TYPE(1010, "Unsupported response type"),
    OAUTH2_ACCESS_DENIED(1011, "Access denied"),
    OAUTH2_UNAUTHORIZED_USER(1012, "Unauthorized user"),
    OAUTH2_USER_DENIED_AUTHORIZATION(1013, "User denied authorization"),
    OAUTH2_BAD_CREDENTIALS(1014, "Bad credentials"),
    AUTH_ACCOUNT_STATUS(1015, "Invalid account_status"),
    AUTH_INSUFFICIENT_AUTHENTICATION(1016, "Insufficient authentication"),
    PRINCIPAL_IS_NULL(1017, "Principal is empty username"),
    OLD_PASSWORD_IS_ERROR(1018, "Old password is error"),



    /**
     * unknown
     * code [8001]
     */
    UNKNOWN_ERROR(8001, "Unknown error");

    private final int code;

    private final String description;

    PlatformError(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public static PlatformError of(int code) {
        return Stream.of(values())
                .filter(e -> e.getCode() == code)
                .findFirst()
                .orElse(SERVER_INTERNAL_ERROR);
    }
}

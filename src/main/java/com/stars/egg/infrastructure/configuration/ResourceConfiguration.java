package com.stars.egg.infrastructure.configuration;

import com.stars.egg.interfaces.api.rest.RestJsonResponseErrorHandler;
import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.OkHttp3ClientHttpRequestFactory;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * User resource server configuration.
 * @author wangs
 */
@Configuration
@EnableResourceServer
public class ResourceConfiguration extends ResourceServerConfigurerAdapter {

    static final String[] PUBLIC_RESOURCES = {
            "/",
            "/api/v1/auth",
            "/api/v1/token",
            "/login",
            "/user/**"
    };

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.antMatcher("/api/**")
                .authorizeRequests()
                .antMatchers(PUBLIC_RESOURCES)
                .permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .csrf()
                .disable();
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.resourceId(AuthServerOauth2Config.RESOURCE_ID);
    }

    @Bean
    public ClientHttpRequestFactory clientHttpRequestFactory(ApplicationProperties properties) {
        ApplicationProperties.RestProperties restProperties = properties.getRestProperties();

        ConnectionPool pool = new ConnectionPool(5, 30, TimeUnit.SECONDS);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectionPool(pool)
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .retryOnConnectionFailure(false)
                .build();
        /*Netty4ClientHttpRequestFactory factory = new Netty4ClientHttpRequestFactory();*/
        OkHttp3ClientHttpRequestFactory factory = new OkHttp3ClientHttpRequestFactory(okHttpClient);
        factory.setConnectTimeout(restProperties.getConnectTimeout());
        /*factory.setMaxResponseSize(restProperties.getMaxResponseSize());*/
        factory.setReadTimeout(restProperties.getReadTimeout());
        return factory;
    }

    @Bean
    public RestTemplate gatewayRestTemplate(
            OAuth2ProtectedResourceDetails resourceDetails,
            ClientHttpRequestFactory clientHttpRequestFactory,
            RestJsonResponseErrorHandler errorHandler) {
        RestTemplate template = new RestTemplate(clientHttpRequestFactory);

        List<ClientHttpRequestInterceptor> interceptors =
                Collections.singletonList(
                        new BasicAuthorizationInterceptor(
                                resourceDetails.getClientId(),
                                resourceDetails.getClientSecret()));

        template.setInterceptors(interceptors);
        template.setErrorHandler(errorHandler);

        return template;
    }
}

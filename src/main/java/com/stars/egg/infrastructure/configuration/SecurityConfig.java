package com.stars.egg.infrastructure.configuration;

import com.stars.egg.infrastructure.oauth2.DelegatingAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Auth server configuration.
 * @author wangs
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private MyAccessDeniedHandler accessDeniedHandler;
    private final DelegatingAuthenticationProvider authenticationProvider;

    @Autowired
    public SecurityConfig(MyAccessDeniedHandler accessDeniedHandler, DelegatingAuthenticationProvider authenticationProvider) {
        this.accessDeniedHandler = accessDeniedHandler;
        this.authenticationProvider = authenticationProvider;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers(ResourceConfiguration.PUBLIC_RESOURCES).permitAll()
                .antMatchers("/admin/**").hasAnyRole("SUPER_ADMIN")
                .antMatchers("/user/**", "/").hasAnyRole("ADMIN", "USER", "GUEST")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .permitAll()
                .and()
                .logout()
                .permitAll()
                .and()
                .exceptionHandling().accessDeniedHandler(accessDeniedHandler);
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

        auth.authenticationProvider(authenticationProvider);
        auth.inMemoryAuthentication()
                .withUser("admin").password("123456").roles("ADMIN")
                .and()
                .withUser("user").password("123456").roles("USER")
                .and()
                .withUser("guest").password("123456").roles("GUEST")
                .and()
                .withUser("super_admin").password("123456").roles("SUPER_ADMIN");
    }

    @Override
    public void configure(WebSecurity web) {
        web
            .ignoring()
            .antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**", "/font-awesome/**", "/webjars/**", "/fonts/**", "/document**");
    }
}

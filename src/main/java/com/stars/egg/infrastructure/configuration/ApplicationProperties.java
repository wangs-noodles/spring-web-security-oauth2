package com.stars.egg.infrastructure.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * @author wangs
 */
@ConfigurationProperties(prefix = "application")
public class ApplicationProperties {

    private ClientDetails clientDetails;


    private RestProperties restProperties;


    public ClientDetails getClientDetails() {
        return clientDetails;
    }

    public void setClientDetails(ClientDetails clientDetails) {
        this.clientDetails = clientDetails;
    }

    public RestProperties getRestProperties() {
        return restProperties;
    }

    public void setRestProperties(RestProperties restProperties) {
        this.restProperties = restProperties;
    }

    public static class ClientDetails{
        private String client;
        private String secret;
        private List<String> scope;
        private List<String> authorizedGrantType;
        private int accessTokenValiditySecondes;
        private int refreshTolenValiditySeconds;

        public String getClient() {
            return client;
        }

        public void setClient(String client) {
            this.client = client;
        }

        public String getSecret() {
            return secret;
        }

        public void setSecret(String secret) {
            this.secret = secret;
        }

        public List<String> getScope() {
            return scope;
        }

        public void setScope(List<String> scope) {
            this.scope = scope;
        }

        public List<String> getAuthorizedGrantType() {
            return authorizedGrantType;
        }

        public void setAuthorizedGrantType(List<String> authorizedGrantType) {
            this.authorizedGrantType = authorizedGrantType;
        }

        public int getAccessTokenValiditySecondes() {
            return accessTokenValiditySecondes;
        }

        public void setAccessTokenValiditySecondes(int accessTokenValiditySecondes) {
            this.accessTokenValiditySecondes = accessTokenValiditySecondes;
        }

        public int getRefreshTolenValiditySeconds() {
            return refreshTolenValiditySeconds;
        }

        public void setRefreshTolenValiditySeconds(int refreshTolenValiditySeconds) {
            this.refreshTolenValiditySeconds = refreshTolenValiditySeconds;
        }

        @Override
        public String toString() {
            return "ClientDetails{" +
                    "client='" + client + '\'' +
                    ", secret='" + secret + '\'' +
                    ", scope=" + scope +
                    ", authorizedGrantType=" + authorizedGrantType +
                    ", accessTokenValiditySecondes=" + accessTokenValiditySecondes +
                    ", refreshTolenValiditySeconds=" + refreshTolenValiditySeconds +
                    '}';
        }
    }

    public static class RestProperties {
        private int connectTimeout;
        private int maxResponseSize;
        private int readTimeout;

        public int getConnectTimeout() {
            return connectTimeout;
        }

        public void setConnectTimeout(int connectTimeout) {
            this.connectTimeout = connectTimeout;
        }

        public int getMaxResponseSize() {
            return maxResponseSize;
        }

        public void setMaxResponseSize(int maxResponseSize) {
            this.maxResponseSize = maxResponseSize;
        }

        public int getReadTimeout() {
            return readTimeout;
        }

        public void setReadTimeout(int readTimeout) {
            this.readTimeout = readTimeout;
        }
    }

    @Override
    public String toString() {
        return "ApplicationProperties{" +
                "clientDetails=" + clientDetails +
                ", restProperties=" + restProperties +
                '}';
    }
}

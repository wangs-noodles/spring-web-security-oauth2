package com.stars.egg.infrastructure.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import org.springframework.security.oauth2.provider.error.OAuth2AuthenticationEntryPoint;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;
import org.springframework.util.Assert;

import java.util.Collections;

/**
 * OAuth2 server configuration.
 *
 * @author wangs
 */
@Configuration
@EnableAuthorizationServer
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class AuthServerOauth2Config extends AuthorizationServerConfigurerAdapter {
    public static final String RESOURCE_ID = "egg-server";

    private final ApplicationContext applicationContext;

    private final UserDetailsService userDetailsService;

    private final AuthenticationProvider authenticationProvider;

    private final ApplicationProperties.ClientDetails clientDetails;

    @Autowired
    public AuthServerOauth2Config(
            ApplicationContext applicationContext,
            UserDetailsService userDetailsService,
            AuthenticationProvider authenticationProvider,
            ApplicationProperties applicationProperties) {
        Assert.notNull(userDetailsService, "delegatingUserDetailsService can not be null");
        Assert.notNull(authenticationProvider, "authenticationProvider can not be null");
        Assert.notNull(applicationProperties, "applicationProperties can not be null");
        this.applicationContext = applicationContext;
        this.userDetailsService = userDetailsService;
        this.authenticationProvider = authenticationProvider;
        this.clientDetails = applicationProperties.getClientDetails();
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security.tokenKeyAccess("permitAll()")
                .checkTokenAccess("isAuthenticated()")
                .checkTokenAccess("permitAll()");
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        RedisConnectionFactory redisConnectionFactory = applicationContext.getBean(RedisConnectionFactory.class);

        endpoints.userDetailsService(userDetailsService)
                .authenticationManager(new ProviderManager(
                        Collections.singletonList(authenticationProvider)))
                .tokenStore(tokenStore(redisConnectionFactory))
                /*whether to reuse refresh tokens (until expired), default true.*/
                .reuseRefreshTokens(false);
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
                .withClient(clientDetails.getClient())
                .secret(clientDetails.getSecret())
                .scopes(clientDetails.getScope().toArray(new String[]{}))
                .authorizedGrantTypes(clientDetails.getAuthorizedGrantType().toArray(new String[]{}))
                .resourceIds(RESOURCE_ID)
                .accessTokenValiditySeconds(clientDetails.getAccessTokenValiditySecondes())
                .refreshTokenValiditySeconds(clientDetails.getRefreshTolenValiditySeconds());
    }

    @Bean
    public TokenStore tokenStore(RedisConnectionFactory redisConnectionFactory) {
        return new RedisTokenStore(redisConnectionFactory);
    }

}

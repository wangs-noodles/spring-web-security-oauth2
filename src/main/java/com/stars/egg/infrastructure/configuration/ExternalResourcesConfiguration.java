package com.stars.egg.infrastructure.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author wangs
 */
@Configuration
public class ExternalResourcesConfiguration extends WebMvcConfigurerAdapter {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //windows
        registry.addResourceHandler("/document/**").addResourceLocations("file:E:/document/");
        //linux
//        registry.addResourceHandler("/document/**").addResourceLocations("file:/app/document/");
        super.addResourceHandlers(registry);
    }

}

package com.stars.egg.infrastructure.persistence.jpa;

import com.stars.egg.domain.account.User;
import com.stars.egg.domain.account.UserRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author wangs
 */
@Repository
public interface UserJpaRepository extends JpaRepository<User, Long>, UserRepository {
    User findByUsername(String username);
}
